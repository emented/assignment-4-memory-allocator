//
// Created by Илья Поветин on 09.12.2022.
//

#define _DEFAULT_SOURCE

#include "malloc_tests.h"

void debug_heap_with_message(void *heap, char *msg) {
    printf("%s %s", msg, "\n");
    debug_heap(stdout, heap);
}

void free_heap(void *heap, size_t length) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = length}).bytes);
}

bool simple_success_memory_allocate_test() {
    printf("Test: simple_success_memory_allocate_test STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap_with_message(heap, "Heap after initialization:");

    if (!heap) {
        printf("Test: simple_success_memory_allocate_test FAILED\n");
        return false;
    }

    void *memory = _malloc(1024);
    debug_heap_with_message(heap, "Heap after alloc:");

    if (!memory) {
        printf("Test: simple_success_memory_allocate_test FAILED\n");
        return false;
    }

    _free(memory);
    debug_heap_with_message(heap, "Heap after free:");

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test: simple_success_memory_allocate_test PASSED\n");
    return true;
}

bool success_free_one_block_out_of_many_allocated_test() {
    printf("Test: success_free_one_block_out_of_many_allocated_test STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap_with_message(heap, "Heap after initialization:");

    if (!heap) {
        printf("Test: simple_success_memory_allocate_test FAILED\n");
        return false;
    }

    void *memory_1 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 1 alloc:");

    void *memory_2 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 2 alloc:");

    void *memory_3 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 3 alloc:");

    if (!memory_1 || !memory_2 || !memory_3) {
        printf("Test: success_free_one_block_out_of_many_allocated_test FAILED\n");
        return false;
    }

    _free(memory_2);
    debug_heap_with_message(heap, "Heap after 2 free:");

    if (!memory_1 || !memory_3) {
        printf("Test: success_free_one_block_out_of_many_allocated_test FAILED\n");
        return false;
    }
    _free(memory_1);
    _free(memory_3);
    debug_heap_with_message(heap, "Heap after free:");

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test: success_free_one_block_out_of_many_allocated_test PASSED\n");
    return true;
}

bool success_free_two_block_out_of_many_allocated_test() {
    printf("Test: success_free_one_block_out_of_many_allocated_test STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap_with_message(heap, "Heap after initialization:");

    if (!heap) {
        printf("Test: success_free_two_block_out_of_many_allocated_test FAILED\n");
        return false;
    }

    void *memory_1 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 1 alloc:");

    void *memory_2 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 2 alloc:");

    void *memory_3 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 3 alloc:");

    if (!memory_1 || !memory_2 || !memory_3) {
        printf("Test: success_free_two_block_out_of_many_allocated_test PASSED\n");
        return false;
    }

    _free(memory_2);
    _free(memory_3);
    debug_heap_with_message(heap, "Heap after 2 and 3 free:");

    if (!memory_1) {
        printf("Test: success_free_two_block_out_of_many_allocated_test FAILED\n");
        return false;
    }
    _free(memory_1);
    debug_heap_with_message(heap, "Heap after free:");

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test: success_free_two_block_out_of_many_allocated_test PASSED\n");
    return true;
}

bool grow_heap_right_after_test() {
    printf("Test: grow_heap_right_after_test STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap_with_message(heap, "Heap after initialization:");

    if (!heap) {
        printf("Test: grow_heap_right_after_test FAILED\n");
        return false;
    }

    void *memory_1 = _malloc(REGION_MIN_SIZE * 2);
    debug_heap_with_message(heap, "Heap after 1 alloc:");

    if (!memory_1) {
        printf("Test: grow_heap_right_after_test FAILED\n");
        return false;
    }

    struct block_header const *heap_header = heap;
    if (heap_header->capacity.bytes <= REGION_MIN_SIZE) {
        printf("Test: grow_heap_right_after_test FAILED\n");
        return false;
    }

    _free(memory_1);
    debug_heap_with_message(heap, "Heap after free:");

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test: grow_heap_right_after_test PASSED\n");
    return true;
}

bool grow_heap_if_right_after_is_not_free_test() {
    printf("Test: grow_heap_if_right_after_is_not_free_test STARTED\n");

    void *heap = heap_init(REGION_MIN_SIZE);
    debug_heap_with_message(heap, "Heap after initialization:");

    if (!heap) {
        printf("Test: grow_heap_right_after_test FAILED\n");
        return false;
    }

    void *memory_1 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 1 alloc:");

    if (!memory_1) {
        printf("Test: grow_heap_if_right_after_is_not_free_test FAILED\n");
        return false;
    }

    struct block_header const *header_1 = (struct block_header*) (memory_1 - offsetof(struct block_header, contents));

    (void) mmap((void *) header_1->contents + header_1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *memory_2 = _malloc(1024);
    debug_heap_with_message(heap, "Heap after 2 alloc:");

    if (!memory_2) {
        printf("Test: grow_heap_if_right_after_is_not_free_test FAILED\n");
        return false;
    }

    _free(memory_1);
    _free(memory_2);
    debug_heap_with_message(heap, "Heap after free:");

    free_heap(heap, REGION_MIN_SIZE);

    printf("Test: grow_heap_if_right_after_is_not_free_test PASSED\n");
    return true;
}



