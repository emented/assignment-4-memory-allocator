//
// Created by Илья Поветин on 09.12.2022.
//

#ifndef MALLOC_TESTS_H
#define MALLOC_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

#define TESTS_COUNT 5

bool simple_success_memory_allocate_test();

bool success_free_one_block_out_of_many_allocated_test();

bool success_free_two_block_out_of_many_allocated_test();

bool grow_heap_right_after_test();

bool grow_heap_if_right_after_is_not_free_test();


#endif //MALLOC_TESTS_H
