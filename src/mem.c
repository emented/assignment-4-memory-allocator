#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    if (addr) {
        *((struct block_header *) addr) = (struct block_header) {
                .next = next,
                .capacity = capacity_from_size(block_sz),
                .is_free = true
        };
    }
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    block_size new_block_size = size_from_capacity((block_capacity) {query});

    size_t new_block_size_bytes = new_block_size.bytes;
    size_t actual_size = region_actual_size(new_block_size_bytes);

    void *allocation_result = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);

    if (allocation_result == MAP_FAILED) {
        allocation_result = map_pages(addr, actual_size, 0);
    }

    void *new_address = allocation_result == MAP_FAILED ? NULL : allocation_result;

    struct region allocated_region = {
            .addr = new_address,
            .size = actual_size,
            .extends = new_address == addr
    };

    if (!region_is_invalid(&allocated_region)) block_init(new_address, (block_size) {actual_size}, NULL);

    return allocated_region;

}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {

    if (!block_splittable(block, query)) return false;

    size_t current_block_capacity_in_bytes = block->capacity.bytes;

    void *new_block = block->contents + query;
    block_size new_block_size = {current_block_capacity_in_bytes - query};

    block_init(new_block, new_block_size, block->next);

    block->capacity.bytes = query;
    block->next = new_block;

    return true;

}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (block->next == NULL || !mergeable(block, block->next)) return false;

    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;

    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {

    if (block == NULL) return (struct block_search_result) {BSR_CORRUPTED};

    struct block_header *next_header = block;

    while (next_header) {

        while (try_merge_with_next(next_header));

        if (next_header->is_free && block_is_big_enough(sz, next_header)) {
            return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, next_header};
        }

        if (next_header->next == NULL) break;

        next_header = next_header->next;
    }

    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, next_header};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {

    query = size_max(query, BLOCK_MIN_CAPACITY);

    struct block_search_result block_search_result = find_good_or_last(block, query);

    if (block_search_result.type == BSR_FOUND_GOOD_BLOCK) {

        split_if_too_big(block_search_result.block, query);
        block_search_result.block->is_free = false;

    }

    return block_search_result;

}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {

    if (last == NULL) return NULL;

    struct region new_region = alloc_region(block_after(last), query);

    if (!region_is_invalid(&new_region)) {

        last->next = new_region.addr;

        if (new_region.extends && try_merge_with_next(last)) {
            return last;
        }

        return new_region.addr;
    }

    return NULL;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {

    if (heap_start == NULL) return NULL;

    query = size_max(query, BLOCK_MIN_CAPACITY);

    struct block_search_result search_result = try_memalloc_existing(query, heap_start);

    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        return search_result.block;
    } else if (search_result.type == BSR_REACHED_END_NOT_FOUND) {
        search_result.block = grow_heap(search_result.block, query);
        search_result = try_memalloc_existing(query, search_result.block);
        return search_result.block;
    } else {
        return NULL;
    }

}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
