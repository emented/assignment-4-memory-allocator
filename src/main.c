//
// Created by Илья Поветин on 09.12.2022.
//

#include "malloc_tests.h"

int main() {

    int successfully_passed_tests = 0;

    successfully_passed_tests += simple_success_memory_allocate_test();
    successfully_passed_tests += success_free_one_block_out_of_many_allocated_test();
    successfully_passed_tests += success_free_two_block_out_of_many_allocated_test();
    successfully_passed_tests += grow_heap_right_after_test();
    successfully_passed_tests += grow_heap_if_right_after_is_not_free_test();

    printf("Passed %d tests out of %d tests\n", successfully_passed_tests, TESTS_COUNT);
    printf("Failed %d tests out of %d tests", TESTS_COUNT - successfully_passed_tests, TESTS_COUNT);
}

